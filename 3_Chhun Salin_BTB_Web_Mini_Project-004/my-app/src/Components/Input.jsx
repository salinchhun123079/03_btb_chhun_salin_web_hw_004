import React, { Component } from 'react'
import Table from './Table'

export class Input extends Component {
    constructor() {
        super()
        this.state = {
            studentName: [
            ],
            newEmail: "",
            newName: "",
            newSex: "",
            newStatus: "",

        }
    }
    handleEmail = (eventEmail) => {
        this.setState({
            newEmail: eventEmail.target.value
        })
    }
    handleName = (event) => {
        this.setState({
            newName: event.target.value
        })
    }
    handleSex = (eventSex) => {
        this.setState({
            newSex: eventSex.target.value
        })
    }

    onSubmit = () => {
        const newObj = {
            id: this.state.studentName.length + 1,
            name: this.state.newName,
            email: this.state.newEmail,
            sex: this.state.newSex,
            status: this.state.newStatus = "Pending"
        }
        this.setState({
            studentName: [...this.state.studentName, newObj],
            newEmail: "",
            newName: "",
            newSex: "",
            newStatus: "",

        })
    }
    onStatusChange = (id) => {
        this.state.studentName.map(data => {
            if (data.id == id) {
                data.status = data.status === "Pending" ? "Done" : "Pending"
            }
        })
        this.setState({
            data: this.state.studentName
        })
    }

    render() {
        return (
            <div>
                <div className='w-1/2 m-auto text-2xl  mt-16 '>
                    <h1 className='text-center text-5xl ' id='fon' ><span className='text-transparent bg-clip-text  bg-gradient-to-r from-blue-500 to-red-600'>Please Fill your</span> information</h1>
                    <label className="block mb-2 text-2xl font-medium text-gray-900 mt-16">Your Email</label>
                    <div class="relative mb-6">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
                        </div>
                        <input type="text" onChange={this.handleEmail} id="input-group-1" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5   dark:border-gray-400 dark:placeholder-gray-400  dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@gmail.com" />
                    </div>
                    <div className="mb-6">
                        <label className="block mb-2 text-2xl font-medium text-gray-900">Username</label>
                        <div class="flex">
                            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-300 dark:text-gray-400 dark:border-gray-400">
                                @
                            </span>
                            <input type="text" onChange={this.handleName} id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5 dark:border-gray-400 dark:placeholder-gray-400  dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Your Name" />
                        </div>
                    </div>
                    <div className="mb-6">
                        <label className="block mb-2 text-2xl font-medium text-gray-900">Age</label>
                        <div class="flex">
                            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-300 dark:text-gray-400 dark:border-gray-400">
                                🤞
                            </span>
                            <input type="text" onChange={this.handleSex} id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5   dark:border-gray-400 dark:placeholder-gray-400  dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Your Age" />
                        </div>
                    </div>
                    <div className='flex  justify-center mb-5'>
                        <button onClick={this.onSubmit} class="relative inline-flex  mt-2 items-center justify-center p-0.5 mb-2 mr-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-purple-500 to-pink-500 group-hover:from-purple-500 group-hover:to-pink-500 hover:text-white focus:ring-4 focus:outline-none focus:ring-purple-200 dark:focus:ring-purple-800">
                            <span class="relative px-16  py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-50 rounded-md group-hover:bg-opacity-0">
                                Register
                            </span>
                        </button>
                    </div>  
                </div>
                <Table data={this.state.studentName} onStatusChange={this.onStatusChange} />
            </div>
        )
    }
}

export default Input