import React, { Component } from 'react'
import Swal from "sweetalert2";
import 'animate.css';


export class Table extends Component {
show=(item)=>{
    Swal.fire({
        title : ` ID : ${item.id} \n Email : ${item.email} \n Username : ${item.name} \n Age : ${item.sex}`,
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      })
}
    render() {
        return (
            <div className='w-1/2 m-auto'>
                <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-300 dark:text-gray-400">
                            <tr className='text-center'>
                                <th scope="col" class="px-6 py-3">
                                    id
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Email
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Username
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Age
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.props.data.map((item) => (
                                <tr key={item.id} className={`text-black ${item.id % 2 === 0 ? `bg-red-200` : `bg-white`} rounded border-b dark:border-gray-700 text-center`}>
                                    <td>{item.id}</td>
                                    <td>{item.email === "" ? "null" : item.email}</td>
                                    <td>{item.name === "" ? "null" : item.name}</td>
                                    <td>{item.sex === "" ? "null" : item.sex}</td>
                                    <td>
                                        <button onClick={() =>this.props.onStatusChange(item.id)}
                                         className={`${item.status === "Pending" ? `focus:outline-none text-white mt-2 w-40 h-10 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-10 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900` : `focus:outline-none text-white mt-2 w-40 h-10  focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-10 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:green:bg-red-700 dark:focus:ring-green-900`}`}
                                           
                                             type="button"  >
                                            {item.status}
                                        </button>
                                        <button type="button" onClick={()=>this.show(item)} class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Show</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }
}

export default Table
